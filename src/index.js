import React from 'react';
import ReactDOM from 'react-dom';

import App from './App'; // Componente root que renderizará todos os componentes filhos

// O ReactDOM.render é responsável por renderizar os componentes na tela
ReactDOM.render(
  /*
  Strict mode: O strict mode é uma flag que "modifica" a maneira que você escreve JS, ele detecta erros silenciosos do JS e os lan-
  ça como exeções, além de evitar equívocos que dificultam os motores JS de fazerem otimizações. Resumindo, ele é um complemento
  que traz boas práticas para o seu código.
  */
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
/*
  O React utiliza esse arquivo index.js para inserir todo o conteúdo JSX do App.js no index.html, tornando possível
  que esse template seja exibido no navegador.Ele faz isso através de um método interno do ReactDOM chamado
  render()(que também utiliza JSX ao passar < App /> como parâmetro).
*/
