import React, { Component } from 'react';
import ListaDeNotas from './components/listaDeNotas'
import FormularioCadastro from './components/formularioCadastro'
import "./assets/App.css";
import './assets/index.css';
// função componente root, que renderizará todo os componentes da aplicação
// JSX Component:
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      notas: []
    }

    this._criarNota = this._criarNota.bind(this);
    this._deletarNota = this._deletarNota.bind(this);
  }
  _criarNota (titulo, texto) {
    const novaNota = { titulo, texto };
    const arrayNotasUpdate = [...this.state.notas, novaNota];
    const novoEstadoNota = {
      notas: arrayNotasUpdate
    }
    this.setState(novoEstadoNota);
    console.log(arrayNotasUpdate);
  }
  _deletarNota (index) {
    let arrayDeNotas = this.state.notas;
    arrayDeNotas.splice(index, 1);
    this.setState(() => ({
      notas: arrayDeNotas
    }))
  }
  render () {
    return (
      <section className="conteudo">
        <FormularioCadastro criarNota={this._criarNota} />
        <ListaDeNotas apagarNota={this._deletarNota} notas={this.state.notas} />
      </section>
    );
  }
}

export default App;

/*
  JSX: O JSX é uma maneira diferente de criar componentes, é uma mistura de template em HTML e a lógica de aplicação em JS como vemos
  no exemplo acima, na função App. Resumindo, o JSX é uma sintaxe para escrever componente semelhante ao XML para melhor compreenção
  do componentes montados. Doc oficial -> https://pt-br.reactjs.org/docs/introducing-jsx.html
*/