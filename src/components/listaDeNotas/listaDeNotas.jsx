import React, { Component } from "react"
import CardNota from "../cardNota"
import "./estilo.css"
class ListaDeNotas extends Component {
  render () {
    return (
      <ul className="lista-notas">
        {
          // quando se apbre duas chaves no JSX, você poderá usar javascript
          this.props.notas.map((nota, index) => {
            return (
              <li className="lista-notas_item" key={index}>
                <CardNota
                  indice={index}
                  apagarNota={this.props.apagarNota}
                  titulo={nota.titulo}
                  texto={nota.texto} />
              </li>
            );
          })
        }
      </ul>
    )
  }
}

export default ListaDeNotas
/**
 * O JSX usa a notação de classes para construção de componentes; para construí-los, precisamos importar o React para usarmos a
 * função de renderização chamada "render()", a classe "Component" permite que a classe declarada seja encarada como componente
 * pelo JSX, além de atribuir funcionalidades a mais a classe.
 */