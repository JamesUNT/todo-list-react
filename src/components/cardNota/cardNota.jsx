import React, { Component } from 'react';
import './estilo.css'
// Importação como img
//import deleteSVG from '../../assets/SVGs/delete.svg

// Importação como SVG; o rótulo do componente deve começar com letra maiúscula
import { ReactComponent as DeleteSVG } from '../../assets/SVGs/delete.svg'
class CardNota extends Component {

  constructor(props) {
    super(props)
    this._apagar = this._apagar.bind(this)
  }

  _apagar () {
    const indice = this.props.indice;
    this.props.apagarNota(indice);
  }
  render () {
    return (
      <section className="card-nota">
        <header className="card-nota_cabecalho">
          <h3 className="card-nota_titulo">{this.props.titulo}</h3>
          {/* <img src={deleteSVG} alt="delete" /> Uso como imagem*/}
          <DeleteSVG onClick={this._apagar} /> {/*Uso como SVG*/}
        </header>
        <p className="card-nota_texto">{this.props.texto}</p>
      </section >
    );
  }
}

export default CardNota;