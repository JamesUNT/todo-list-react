import React, { Component } from "react"
import "./estilo.css"
class FormularioCadastro extends Component {

  // Construtor para instanciar atributos que será usado na classe
  // Atraves das "props" temos acesso as propriedades do componentekpor
  constructor(props) {
    super(props); // Super é necessário, já que estamos estendendo da classe componente
    this.titulo = "";
    this.texto = "";
    // O state é um campo especial do contructor pois é nele em que armazenamos as informações de estádo do componente
    this.state = {
      counter: 0
    };
    /*
    porque usar o bind(this) ?
    Por que no JS, o "this" tem um contexto dinâmico, ou seja, se você usar o "this" dentro de uma função ou no caso um
    "event handle" o "this" tem o contexto do "onChange". Para evitar isso, precisamos usar o método bind() passando
    como parâmetro o "this" que faz referência ao contexto da classe e não do método.

    DICA: você só precisará fazer esse processo se houver um "this" dentro da função, e se elá esta associada a alguma tag jsx,
    e deve ser DENTRO do construtor
    */
    //Padronização de "data binding"
    this._handleMudancaDeTitulo = this._handleMudancaDeTitulo.bind(this);
    this._handleMudancaDeTexto = this._handleMudancaDeTexto.bind(this);
    this._incremento = this._incremento.bind(this);
    this._criarNota = this._criarNota.bind(this);
  }
  //Methods
  _handleMudancaDeTitulo (evento) {
    this.titulo = evento.target.value;
    console.log(this.titulo);
  }
  _handleMudancaDeTexto (evento) {
    this.texto = evento.target.value;
    console.log(this.texto);
  }
  _incremento () {
    this.setState(state => ({
      counter: state.counter + 1
    }));
  }
  _criarNota (evento) {
    evento.preventDefault()
    evento.stopPropagation()
    this.props.criarNota(this.titulo, this.texto)
  }
  render () {
    return (
      <form
        className="form-cadastro"
        onSubmit={this._criarNota}
      >
        <input
          type="text"
          placeholder="Título"
          className="form-cadastro_input"
          onChange={this._handleMudancaDeTitulo}
        />
        <textarea
          rows={15}
          placeholder="Escreva sua nota..."
          className="form-cadastro_input"
          onChange={this._handleMudancaDeTexto}
        />
        <button
          className="form-cadastro_input form-cadastro_submit"
        >
          Criar Nota
        </button>
      </form>
    )
  }
}

export default FormularioCadastro